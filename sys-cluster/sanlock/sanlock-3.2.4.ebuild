# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
inherit eutils

DESCRIPTION="SAN Lock Manager"
HOMEPAGE="https://fedorahosted.org/sanlock/"
SRC_URI="https://git.fedorahosted.org/cgit/sanlock.git/snapshot/${P}.tar.gz"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="dev-libs/libaio
    sys-apps/util-linux"
DEPEND="${DEPEND}"

src_compile() {
    emake -C wdmd

    emake -C src
}

src_install() {
    emake -C wdmd install DESTDIR="${ED}"
    emake -C src install DESTDIR="${ED}"
}
